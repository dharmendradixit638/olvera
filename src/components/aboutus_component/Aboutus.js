function Aboutus() {

    return (
        <div className="row_100 aboutusCntr bg_color_code_1">
            <div className="opacityBox aboutusGradient"></div>
            <div className="container">
                <div className="row">
                    <div className="col">
                        <h2>About Olvera</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing<br /> elit, sed do eiusmod tempor incididunt ut labore et<br /> dolore magna aliqua.</p>
                    </div>
                </div>
                <ul className="row_100 aboutSlider">
                    <li className="col"><img src={process.env.PUBLIC_URL + '/about_slider_1.png'} alt="slide 1" /></li>
                    <li className="col"><img src={process.env.PUBLIC_URL + '/about_slider_2.png'} alt="slide 2" /></li>
                    <li className="col"><img src={process.env.PUBLIC_URL + '/about_slider_3.png'} alt="slide 3" /></li>
                    <li className="col"><img src={process.env.PUBLIC_URL + '/about_slider_4.png'} alt="slide 4" /></li>
                </ul>
            </div>
        </div>
    );
}

export default Aboutus;
