import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import Header from './components/header_component/Header';
import Home from './components/home_component/Home';
import Aboutus from './components/aboutus_component/Aboutus';
import Services from './components/services_component/Services';
import Experience from './components/experience_component/Experience';
import Resources from './components/resources_component/Resources';
import Contactus from './components/contactus_component/Contactus';
import Notfound from './components/notfound_component/Notfound';
import Footer from './components/footer_component/Footer';

function App() {
  return (
    <div className="App">
      <Router>
        <Header />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/home" element={<Home />} />
          <Route path="/aboutus" element={<Aboutus />} />
          <Route path="/services" element={<Services />} />
          <Route path="/experience" element={<Experience />} />
          <Route path="/resources" element={<Resources />} />
          <Route path="/contactus" element={<Contactus />} />
          <Route path="*" element={<Notfound />} />
        </Routes>
        <Footer />

      </Router>
    </div>
  );
}

export default App;
