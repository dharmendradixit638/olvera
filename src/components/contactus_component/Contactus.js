import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css';

function Contactus() {
    return (
        <>

            <div className="row_100 videoCntr pos_relative">
                <div className="opacityBox videoGradient"></div>
                <div className="container pos_relative">
                    <span><i class="fa fa-play" aria-hidden="true"></i></span>
                </div>
            </div>


            <div className="row_100 clientsCntr pt-5 pb-5">
                <div className="container">
                    <div className="row pb-4">
                        <h2 className="pb-5">What our clients say about us</h2>
                        <div className="row mob_m_0 pt-3 sliderArea">
                            <Slide slidesToScroll={1} slidesToShow={1} responsive={[{
                                breakpoint: 800,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 3
                                }
                            }, {
                                breakpoint: 500,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 3
                                }
                            }]}>
                                <div class="itemBox">
                                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam"</p>
                                </div>


                                <div class="itemBox">
                                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam"</p>
                                </div>


                                <div class="itemBox">
                                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam"</p>
                                </div>

                            </Slide>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default Contactus;
