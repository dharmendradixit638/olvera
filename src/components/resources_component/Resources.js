function Resources() {

    return (
        <div className="row_100 careerCntr pos_relative">
            <img className='row_100' src={process.env.PUBLIC_URL + '/career_banner.jpeg'} alt="Logo" />

            <div className="container pb-5">
                <div className="row">
                    <div class="careerArea pt-5 pb-5">
                        <h4 className="pt-5">CAREERS</h4>
                        <h3 className="pb-3">Careers at Olvera</h3>
                        <p className="pb-5">Navigate change, develop on-demand skills. Your turnaround journey starts with Olvera.</p>
                        <a href="#">Search Careers</a>                        </div>
                </div>
            </div>
        </div>
    );
}

export default Resources;
