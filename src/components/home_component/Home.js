import Aboutus from "../aboutus_component/Aboutus";
import Services from "../services_component/Services";
import Experience from "../experience_component/Experience";
import Resources from "../resources_component/Resources";
import Contactus from "../contactus_component/Contactus";

function Home() {
    return (
        <section>
            <div className="row_100 startYourstoryCntr pos_relative">
                <div className="opacityBox startGradient"></div>
                <div className="container">
                    <div className="startBox pos_absolute">
                        <div className="midBox">
                            <h2>Start your story<br /> with Olvera</h2>
                            <a className="btn btn-primary btnStyle" href="#">CONTACT US</a>
                        </div>
                    </div>
                </div>
            </div>

            <Aboutus />

            <Services />

            <Experience />


            <Resources />

            <Contactus />
        </section>
    );
}

export default Home;
