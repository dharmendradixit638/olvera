function Experience() {

    return (
        <div className="row_100 guideCntr pos_relative pt-5 pb-5">
                <div className="opacityBox guideGradient"></div>
                <div className="container pos_relative">
                    <h2 className="pt-5">Olvera Guides</h2>
                    <a href="#">View More <span><i className="fa fa-arrow-right"></i></span></a>
                    <p className="row_100 pt-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt<br /> ut labore et dolore magna aliqua.</p>
                </div>
            </div>
    );
}

export default Experience;
