import React, { useState, useEffect, useRef } from 'react';
import { Link } from 'react-router-dom';


function Header() {
    const [scrolled, setScrolled] = useState(false);
    const [notReg, setNotReg] = useState(false);
    const [mobMenu, setMobMenu] = useState(false);
    const wrapperRef = useRef(null);

    useEffect(() => {
        setMobMenu(mobMenu);
        window.addEventListener('scroll', handleScroll);
        document.addEventListener("mousedown", handleClick);
        return (() => {
            window.removeEventListener('scroll', handleScroll);
            document.removeEventListener("mousedown", handleClick);
        })
    }, [mobMenu]);

    const handleScroll = () => {
        window.scrollY > 0 ? setScrolled(true) : setScrolled(false);
    }

    const handleClick = e => {
        if (wrapperRef.current && !wrapperRef.current.contains(e.target)) {
            setMobMenu(false);
            setNotReg(false);
        }
    }

    const gonextHide = (cls, idd) => {
        setMobMenu(false);
        if (document.querySelector('.' + cls + '.active') != null) {
            document.querySelector('.' + cls + '.active').classList.remove('active');
        }
        document.querySelector('#' + idd).classList.add('active');
    }

    const rmvActiveFun = (cls) => {
        if (document.querySelector('.' + cls + '.active') != null) {
            document.querySelector('.' + cls + '.active').classList.remove('active');
        }
    }

    const mobileAction = () => {
        setMobMenu(true);
        mobMenu == true ? setMobMenu(false) : setMobMenu(true);
    }
    const notreg = () => {
        setNotReg(!notReg);
    }

    return (
        <header className={`${scrolled == true ? 'active' : ''} headerCntr pos_relative`}>
            <div className='row_100 bg_color_code_1 pt-2 pb-2'>
                <div className='container'>
                    <div className='row_100 text-center'>
                        <p className='mb-0 px-0 topTitle text-white montserrat-alternates-font font-w-800 text-uppercase letter-spac-2'>Turning Uncertainity Into your advantage</p>
                    </div>
                </div>
            </div>
            <div className='row_100 mobResponsive bg_color_code_2 pt-4 pb-4'>
                <div className="container">
                    <div className="row_100">
                        <div className="logoBox">
                            <Link to="/" onClick={() => rmvActiveFun('mLink')}>
                                <img  src={process.env.PUBLIC_URL + '/logo.png'} alt="Logo" />
                            </Link>
                        </div>
                        <span className="mobileIcon" onClick={mobileAction}>
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                        <div ref={wrapperRef} className={`${mobMenu == true ? 'active' : ''} menuBox`}>
                            <ul className="row_100">
                                <li><Link className="mLink" id="home" onClick={() => gonextHide('mLink', 'home')} to="/home">Home<span></span></Link></li>
                                <li><Link className="mLink" id="aboutus" onClick={() => gonextHide('mLink', 'aboutus')} to="/aboutus">About Us <span></span></Link></li>
                                <li><Link className="mLink" id="services" onClick={() => gonextHide('mLink', 'services')} to="/services" >Our services <span></span></Link></li>
                                <li><Link className="mLink" id="experience" onClick={() => gonextHide('mLink', 'experience')} to="/experience" >Our experience <span></span></Link></li>
                                <li><Link className="mLink" id="resources" onClick={() => gonextHide('mLink', 'resources')} to="/resources" >Resources <span></span></Link></li>
                                <li><Link className="mLink contactus" id="contactus" onClick={() => gonextHide('mLink', 'contactus')} to="/contactus" >Contact Us <span></span></Link></li>

                                <li>
                                    <a className="mLink sociallink" id="headerfb"><i className="fab fa-facebook"></i></a>
                                    <a className="mLink sociallink" id="headerig"><i className="fab fa-instagram"></i></a>
                                    <a className="mLink sociallink" id="headerli"><i className="fab fa-linkedin"></i></a>
                                </li>
                            </ul>


                        </div>
                    </div>
                </div>
            </div>
        </header>
    );
}

export default Header;
