import { useEffect, useState } from "react";
import counttotarget from 'counter_up_to_an_target';

function Services() {
    const [counter, setCounter] = useState([
        { name: 'Projects', data_target: '150', icon: '+' },
        { name: 'Clients', data_target: '80', icon: '+' },
        { name: 'Professionals', data_target: '200', icon: '+' },
        { name: 'Clients', data_target: '97', icon: '%' }
    ])

    useEffect(() => {
        counttotarget('counter');
    }, [])

    return (
        <div className="row_100 serviceCntr pt-5 pb-5">
            <div className="container">
                {
                    counter.length > 0 ?
                        (<div className="row counterBox pb-5">
                            {counter.map((item, index) => (
                                <div key={`counter_${index}_${item.name}`} className="col animateBox">
                                    <h3><span id={`counter_${index}_${item.name}`} className="counter" data_target={item.data_target}>0</span>{item.icon}</h3>
                                    <p>{item.name}</p>
                                </div>
                            ))}
                        </div>)
                        : ''
                }

                <div className="row servicesArea pt-3">
                    <div class="col-md-5">
                        <h4>SERVICES</h4>
                        <h3 className="pb-3">Our services that can help your business</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt</p>
                    </div>

                    <div class="col-md-7">
                        <ul>
                            <li>
                                <a><span>01</span>Sustainability<em></em> <i className="fa fa-arrow-right"></i></a>
                            </li>

                            <li>
                                <a><span>02</span>Small Business <em></em><i className="fa fa-arrow-right"></i></a>
                            </li>
                            <li>
                                <a><span>03</span>Forensic Services <em></em><i className="fa fa-arrow-right"></i></a>
                            </li>
                            <li>
                                <a><span>04</span>Risk Management <em></em><i className="fa fa-arrow-right"></i></a>
                            </li>
                            <li>
                                <a><span>05</span>CFO Advisory <em></em><i className="fa fa-arrow-right"></i></a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    );
}

export default Services;
