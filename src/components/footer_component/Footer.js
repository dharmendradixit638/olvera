import { Link } from 'react-router-dom';

function Footer() {

    const gonextHide = (cls, idd) => {
        if (document.querySelector('.' + cls + '.active') != null) {
            document.querySelector('.' + cls + '.active').classList.remove('active');
        }
        document.querySelector('#' + idd).classList.add('active');
    }

    return (
        <footer className="row_100 bg_footer pt-5 ">
            <div className="container">
                <div className='row text-center footerTop'>
                    <div className='col'>
                        <h3 className='text-white row_100'>We are open and accepting to all. Come work with us</h3>
                        <p className='row_100 pt-3 pb-3'>Experts teach you everything from the comfort of your own home. Improve your career today by enrolling in excellent courses at affordable prices.</p>
                        <div className='row_100'>
                            <a href='#'>Get Started</a>
                        </div>
                    </div>
                </div>
                <div className="row footerBottom pt-4 pb-4">
                    <div className="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div className="row_100 footerLogo">
                            <img src={process.env.PUBLIC_URL + '/footer_logo.png'} alt="adtiologo" />
                            <p className='pt-3'>Turning Uncertainty Into Your Advantage</p>
                        </div>
                    </div>

                    <div className="col-xs-12 col-sm-3 col-md-3 col-lg-3 footerUrl">
                        <h3 className='pb-3'>Resources </h3>
                        <ul className="row_100">
                            <li><Link className="mLink" id="home" onClick={() => gonextHide('mLink', 'home')} to="/home">Home<span></span></Link></li>
                            <li><Link className="mLink" id="aboutus" onClick={() => gonextHide('mLink', 'aboutus')} to="/aboutus">About Us <span></span></Link></li>
                            <li><Link className="mLink" id="services" onClick={() => gonextHide('mLink', 'services')} to="/services" >Our services <span></span></Link></li>
                            <li><Link className="mLink" id="experience" onClick={() => gonextHide('mLink', 'experience')} to="/experience" >Our experience <span></span></Link></li>
                            <li><Link className="mLink" id="resources" onClick={() => gonextHide('mLink', 'resources')} to="/resources" >Resources <span></span></Link></li>
                            <li><Link className="mLink contactus" id="contactus" onClick={() => gonextHide('mLink', 'contactus')} to="/contactus" >Contact Us <span></span></Link></li>


                        </ul>
                    </div>

                    <div className="col-xs-12 col-sm-3 col-md-3 col-lg-3 footerUrl">
                        <h3 className='pb-3'>Careers </h3>
                        <ul className="row_100">
                            <li><Link className="mLink" id="home">Olevera academy<span></span></Link></li>
                            <li><Link className="mLink" id="aboutus">Privacy Policy <span></span></Link></li>
                            <li><Link className="mLink" id="services" >Cookies <span></span></Link></li>
                        </ul>
                    </div>

                    <div className="col-xs-12 col-sm-2 col-md-2 col-lg-2 footerUrl">
                        <p>Liability Limited By A Scheme Approved Under Professional Services Scheme</p>
                        <p>Phone: (64) 09 973 4905 </p>
                        <p>Email: ezibuyenquiries@olveraadvisors.com  </p>
                        <p>Website: xyz.com  </p>
                    </div>



                </div>
            </div>

            <div className='row_100 copyRight'>
                <div className="container">


                    <p>© 2023 olevra advisors. All rights reserved</p>
                    <ul>

                        <li>
                            <a className="mLink sociallink" id="headerfb"><i className="fab fa-facebook"></i></a>
                            <a className="mLink sociallink" id="headerig"><i className="fab fa-instagram"></i></a>
                            <a className="mLink sociallink" id="headerli"><i className="fab fa-linkedin"></i></a>
                            <a className="mLink sociallink" id="headerli"><i className="fab fa-twitter"></i></a>
                        </li>
                    </ul>


                </div>
            </div>

        </footer >
    );
}

export default Footer;